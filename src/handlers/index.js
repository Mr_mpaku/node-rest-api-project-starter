const auth = require('./auth.js');
const user = require('./user.js');
const video = require('./video.js');

module.exports = { auth, user, video };