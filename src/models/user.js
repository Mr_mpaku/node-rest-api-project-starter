const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true,
    },
    lastName: {
        type: String,
        required: true,
        trim: true,
    },
	email: {
        type: String,
        unique: true,
        trim: true,
        required: [true, 'Email address is required'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,'Please fill a valid email address'],
    },
    password: {
        type: String,
        required: true,
    },
    resetToken: String,
    resetTokenExpiry: Number,
});

// Hashing the password before saving it to the database
userSchema.pre('save', async function(next) {
    try {
        const salt = await bcrypt.genSalt(10);
        this.password = await bcrypt.hash(this.password, salt);
        next();
    } catch (error) {
        next(error);
    }
});

// Validating the password
userSchema.methods.validatePassword = async function(password) {
    return bcrypt.compare(password, this.password);
};

// Generating Token
userSchema.methods.generateToken = async function() {
    const user = this;
    const payload = {
        userId: user._id,
        email: user.email,
    };
    return jwt.sign(payload, process.env.APP_SECRET, {
        expiresIn: '1d',
    });
};

userSchema.plugin(timestamps);

const User = mongoose.model('User', userSchema, 'Users');

module.exports = User;
