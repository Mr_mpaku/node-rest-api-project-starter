const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const VideoSchema = new Schema({
	index:          String,
    title:          String,
	description:    String,
    url:            String,
	duration:       String,
	type:           String,
	course:         String
});
	
const Video = mongoose.model('video',VideoSchema);

module.exports = Video;


