const User = require('./user.js');
const Video = require('./video.js');

module.exports = { User, Video };