require('dotenv').config();
const express = require('express');

// Express App
const app = express();

// Config
require('./config/db.js')();

// Middlewares
require('./middleware/cookieParser.js')(app);
// require('./middleware/auth.js')(app);
require('./middleware/cors.js')(app);
// require('./middleware/loggedIn.js')(app);
require('./middleware/bodyParser.js')(app);
require('./middleware/morgan.js')(app);
require('./middleware/error.js')(app);

// Routes
// require('./routes/auth.js')(app);
// require('./routes/user.js')(app);
require('./routes/video.js')(app);

module.exports = app;
