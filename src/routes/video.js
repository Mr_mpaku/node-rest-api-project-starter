const { video } = require('../handlers');

module.exports = function(app) {

    // Retrieve all Users
    app.get('/videos', video.fix);

};
