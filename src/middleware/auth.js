const jwt = require('jsonwebtoken');

// Decode the jwt so we can get the userId Id on each request, 
// Then verify incoming token before the request hits the handlers
module.exports = function(app) {
    app.use(async (req, res, next) => {
        const { token } = req.cookies;
        if (token) {
            try {
                const { userId } = await jwt.verify(token, process.env.APP_SECRET);
                // Put the userId onto the reqest for future requests to access
                req.userId = userId;
            } catch (error) {
                throw new Error('Your session expired. Sign in again.');
            }
        }
        next();
    });
};
