const morgan = require('morgan');
const winston = require('../config/winston.js');

module.exports = function(app) {
    app.use(morgan('combined', { stream: winston.stream }));
};
