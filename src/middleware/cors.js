const cors = require('cors');

module.exports = function(app) {
    app.use(
        cors({
            origin: "*",
            credentials: true,
            optionsSuccessStatus: 200,
        })
    );
};
